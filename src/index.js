import { quizData } from "./utils/quizData.js";

const askQuestion = (message, viewType) => {
    if ( viewType === 'boolean') {
        return confirm(message);
    }

    return prompt(message);
}

const userData = quizData.reduce((acc, item) => {
    const result = askQuestion(item.question, item.viewType);

    return String(result).trim().toLowerCase() === String(item.answer) ? acc + 10 : acc;
}, 0);

alert(`Ваш результат: ${userData} баллов!`);
