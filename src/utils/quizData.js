export const quizData = [
    {
        question: 'Сколько хромосом у здорового человека?',
        answer: '46',
        viewType: 'default',
    },
    {
        question: 'Путин - хуйло?',
        answer: true,
        viewType: 'boolean',
    },
    {
        question: 'Сколько хромосом у Путина?',
        answer: '47',
        viewType: 'default',
    },
    {
        question: 'Сколько тупых овец в московии (в млн)?',
        answer: '144',
        viewType: 'default',
    },
    {
        question: 'Снести ли памятник Екатерине-2 в Одессе?',
        answer: true,
        viewType: 'boolean',
    },
    {
        question: 'Сколько черных пакетов выделяются на одного орка?',
        answer: '1',
        viewType: 'default',
    },
    {
        question: 'На сколько вы оцениваете работу ЗСУ от 1 до 10?',
        answer: '10',
        viewType: 'default',
    },
    {
        question: 'Со скольких позиций готовилось нападение на Беларусь?',
        answer: '4',
        viewType: 'default',
    },
    {
        question: 'Нужно ли сжигать сосийский флаг?',
        answer: true,
        viewType: 'boolean',
    },
    {
        question: 'Поддерживаете ли вы уход иностранных компаний из московии?',
        answer: true,
        viewType: 'boolean',
    },
    {
        question: 'Считаете ли вы сосию своим домом?',
        answer: false,
        viewType: 'boolean',
    }
]
